function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}
var total = 0;
var totalBill = 0;
// var toastMixin = Swal.mixin({
// 	toast: true,
// 	animation: false,
// 	position: "top-end",
// 	showConfirmButton: false,
// 	timer: 5000,
// 	timerProgressBar: true,
// });
$('.add-to-cart').click(function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  var name = $(this).data('name');
  var price = $(this).data('price');
  var unit = $(this).data('unit');
  var quantity = Number(prompt('Số lượng bao nhiêu?'));
  while (isNaN(quantity)) {
    quantity = Number(prompt('Số lượng bao nhiêu?'));
  }
  if (quantity > 0 && quantity < 100) {
    total = price * quantity;

    totalBill += total;
    var no = $('tbody.detail-list').children().length + 1;
    if ($(`tr[data-id='${id}']`).length > 0) {
      var currentquantity = Number($(`tr[data-id='${id}'] td.quantity`).text());
      var resultquantity = currentquantity + quantity;

      var currenttotal = Number(
        $(`tr[data-id='${id}'] td.total`).text().replaceAll(',', ''),
      );
      var resulttotal = currenttotal + total;

      $(`tr[data-id='${id}'] td.quantity`).text(resultquantity);
      $(`tr[data-id='${id}'] td.total`).text(formatNumber(resulttotal));
    } else {
      var html = `
        <tr data-id="${id}">
            <td scope="row">${no}</td>
            <td>${name}</td>
            <td class='text-center'>${unit}</td>
            <td class='text-center'>${formatNumber(price)}</td>
            <td class='text-center quantity'>${quantity}</td>
            <td class='text-center total'>${formatNumber(total)}</td>
        </tr> 
    `;
      $('tbody.detail-list').append(html);
    }

    $('td.total-bill').text(formatNumber(totalBill));
  } else if (quantity < 0) {
    alert('Số lượng không được nhỏ hơn 0');
  } else if(quantity>100) {
    // return toastMixin.fire({
    //   icon: "error",
    //   animation: true,
    //   title:
    //     "Số lượng mỗi món không quá 100",
    // });
    alert('Số lượng mỗi món phải khác 0');
  }
});

const menuBtns = document.querySelectorAll('.menu-btn');
menuBtns.forEach((btn) => {
  btn.addEventListener('click', () => {
    resetActiveBtn();
    //showFood(btn.id);
    console.log(btn.id);
    btn.classList.add('active-btn');
  });
});

function resetActiveBtn() {
  menuBtns.forEach((btn) => {
    btn.classList.remove('active-btn');
    //console.log('Success!');
  });
}

$('.categories').click(function (e) {
  const type = $(this).data('type');
  if (type == 'all') {
    $('.col-md-3').each(function () {
      $(this).show();
    });
  } else {
    $(`.col-md-3[data-categories=${type}]`).each(function () {
      $(this).show();
    });
    $(`.col-md-3[data-categories!=${type}]`).each(function () {
      $(this).hide();
    });
  }
  $('.col-md-3[data-type="cart"]').show();
});
